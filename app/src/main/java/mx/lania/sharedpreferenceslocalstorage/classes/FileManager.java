package mx.lania.sharedpreferenceslocalstorage.classes;

import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class FileManager extends AppCompatActivity {

    public String saveFile(){
        //Acceder a shared preferences
        SharedPreferences appPrefs = getSharedPreferences("personalInfoPrefer", MODE_PRIVATE);
        //Limpiar el archivo (Se puede usar MODE_PRIVATE para evitar el uso de esto)
        cleanFile();

        //Se contruye la info a guradar
        String data = appPrefs.getString("namePreference", "").trim() + ",";
        data += appPrefs.getString("lastnamePreference", "").trim() + ",";
        data += appPrefs.getString("surnamePreference", "").trim() + ",";
        data += appPrefs.getString("agePreference", "").trim() + ",";
        data += appPrefs.getString("addressPreference", "").trim() + ",";
        data += appPrefs.getString("phonePreference", "").trim();

        try {
            //Se guarda en el archivo
            FileOutputStream fileOutput = openFileOutput("PersonalInfo.txt", MODE_APPEND);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutput);
            writer.write(data);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
            return "Error al guardar archivo";
        }
        return "Archivo guardado correctamente...";
    }

    public String cleanFile(){
        try {
            //Se limpia el archivo
            FileOutputStream fileOutput = openFileOutput("PersonalInfo.txt", MODE_PRIVATE);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutput);
            writer.write("");
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
            return "Error al guardar archivo";
        }
        return "Archivo guardado correctamente...";
    }
}
