package mx.lania.sharedpreferenceslocalstorage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnLoadPreferences;
    private Button btnShowPrefer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Enlazar elementos de la view
        btnLoadPreferences = (Button) findViewById(R.id.btnLoadPreferences);
        btnShowPrefer = (Button) findViewById(R.id.btnShowPrefer);

        //Accion del boton de cargar preferencias
        btnLoadPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PersonalInfoPreferences.class);
                startActivity(intent);
            }
        });

        //Accion del boton mostrar/editar preferencias
        btnShowPrefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ShowEditInfo.class);
                startActivity(intent);
            }
        });

    }


    private void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}