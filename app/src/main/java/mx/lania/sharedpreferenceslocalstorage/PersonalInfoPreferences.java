package mx.lania.sharedpreferenceslocalstorage;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import mx.lania.sharedpreferenceslocalstorage.classes.FileManager;

public class PersonalInfoPreferences extends PreferenceActivity {

    //private FileManager fm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Inicializar fm
        //fm = new FileManager();

        //Remplazar view por view de SharedPreference
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PersonalInfoFragment()).commit();
    }

    public static class PersonalInfoFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            //Manejo del shared preference
            PreferenceManager preferenceManager = getPreferenceManager();
            preferenceManager.setSharedPreferencesName("personalInfoPrefer");
            addPreferencesFromResource(R.xml.information_preference);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Guardar archivo
        saveFile();
    }

    public boolean saveFile(){
        //Acceder a shared preferences
        SharedPreferences appPrefs = getSharedPreferences("personalInfoPrefer", MODE_PRIVATE);
        //Limpiar el archivo (Se puede usar MODE_PRIVATE para evitar el uso de esto)
        cleanFile();

        //Se contruye la info a guradar
        String data = appPrefs.getString("namePreference", "").trim() + ",";
        data += appPrefs.getString("lastnamePreference", "").trim() + ",";
        data += appPrefs.getString("surnamePreference", "").trim() + ",";
        data += appPrefs.getString("agePreference", "").trim() + ",";
        data += appPrefs.getString("addressPreference", "").trim() + ",";
        data += appPrefs.getString("phonePreference", "").trim();

        try {
            //Se guarda en el archivo
            FileOutputStream fileOutput = openFileOutput("Persona.txt", MODE_APPEND);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutput);
            writer.write(data);
            writer.flush();
            writer.close();

            showMsg("Archivo guardado correctamente...");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean cleanFile(){
        try {
            //Se limpia el archivo
            FileOutputStream fileOutput = openFileOutput("Persona.txt", MODE_PRIVATE);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutput);
            writer.write("");
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}
