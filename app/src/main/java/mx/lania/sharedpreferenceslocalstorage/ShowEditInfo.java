package mx.lania.sharedpreferenceslocalstorage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ShowEditInfo extends AppCompatActivity {

    private EditText nameTxt;
    private EditText lastnameTxt;
    private EditText surnameTxt;
    private EditText ageTxt;
    private EditText addressTxt;
    private EditText phoneTxt;
    private Button btnUpdate;
    private SharedPreferences appPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_edit_info);

        //Enlazar elemntos de la view
        nameTxt =  (EditText) findViewById(R.id.nameTxt);
        lastnameTxt =  (EditText) findViewById(R.id.lastnameTxt);
        surnameTxt =  (EditText) findViewById(R.id.surnameTxt);
        ageTxt =  (EditText) findViewById(R.id.ageTxt);
        addressTxt =  (EditText) findViewById(R.id.addressTxt);
        phoneTxt =  (EditText) findViewById(R.id.phoneTxt);
        btnUpdate =  (Button) findViewById(R.id.btnUpdate);

        //Llenar los campos de preferencias
        this.appPrefs = getSharedPreferences("personalInfoPrefer", MODE_PRIVATE);
        nameTxt.setText(appPrefs.getString("namePreference", ""));
        lastnameTxt.setText(appPrefs.getString("lastnamePreference", ""));
        surnameTxt.setText(appPrefs.getString("surnamePreference", ""));
        ageTxt.setText(appPrefs.getString("agePreference", ""));
        addressTxt.setText(appPrefs.getString("addressPreference", ""));
        phoneTxt.setText(appPrefs.getString("phonePreference", ""));

        //Evento del boton actualizar
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor prefsUpdater = appPrefs.edit();
                prefsUpdater.putString("namePreference", ((EditText) findViewById(R.id.nameTxt)).getText().toString());
                prefsUpdater.putString("lastnamePreference", ((EditText) findViewById(R.id.lastnameTxt)).getText().toString());
                prefsUpdater.putString("surnamePreference", ((EditText) findViewById(R.id.surnameTxt)).getText().toString());
                prefsUpdater.putString("agePreference", ((EditText) findViewById(R.id.ageTxt)).getText().toString());
                prefsUpdater.putString("addressPreference", ((EditText) findViewById(R.id.addressTxt)).getText().toString());
                prefsUpdater.putString("phonePreference", ((EditText) findViewById(R.id.phoneTxt)).getText().toString());
                prefsUpdater.apply();

                saveFile();
                showMsg("Preferencias actualizadas");
            }
        });

    }

    public boolean saveFile(){
        //Acceder a shared preferences
        SharedPreferences appPrefs = getSharedPreferences("personalInfoPrefer", MODE_PRIVATE);
        //Limpiar el archivo (Se puede usar MODE_PRIVATE para evitar el uso de esto)
        cleanFile();

        //Se contruye la info a guradar
        String data = appPrefs.getString("namePreference", "").trim() + ",";
        data += appPrefs.getString("lastnamePreference", "").trim() + ",";
        data += appPrefs.getString("surnamePreference", "").trim() + ",";
        data += appPrefs.getString("agePreference", "").trim() + ",";
        data += appPrefs.getString("addressPreference", "").trim() + ",";
        data += appPrefs.getString("phonePreference", "").trim();

        try {
            //Se guarda en el archivo
            FileOutputStream fileOutput = openFileOutput("Persona.txt", MODE_APPEND);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutput);
            writer.write(data);
            writer.flush();
            writer.close();

            //showMsg("Archivo guardado correctamente...");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean cleanFile(){
        try {
            //Se limpia el archivo
            FileOutputStream fileOutput = openFileOutput("Persona.txt", MODE_PRIVATE);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutput);
            writer.write("");
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}